package com.example.asus.witplex;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.asus.witplex.models.UserModel;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONObject;

import java.io.InputStream;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
    private Activity activity = this;
    private CallbackManager callbackManager;
    private UserModel userModel;
    private ImageView profile_picture;
    private TextView helloName;
    private LoginButton loginButton;
    private Button accountsButton;

    private SharedPreferences loginPreferences;
    private SharedPreferences.Editor loginPreferencesEditor;
    private long userRememberMe = 0L;

    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener firebaseAuthListener;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        profile_picture = findViewById(R.id.profile_picture);
        helloName = findViewById(R.id.helloName);
        accountsButton = findViewById(R.id.accounts_button);
        accountsButton.setOnClickListener(this);
        loginButton = findViewById(R.id.login_button);
        loginButton.setReadPermissions("email", "public_profile");

        loginPreferences = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        loginPreferencesEditor = loginPreferences.edit();
        userRememberMe = loginPreferences.getLong("userStatus", 0);
        if (userRememberMe != 0) {
            new LoadProfileImage(profile_picture).execute(loginPreferences.getString("profile_picture", ""));
            helloName.setText("Hello " + loginPreferences.getString("first_name", "") + "!");
        }

        callbackManager = CallbackManager.Factory.create();
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                handleFacebookAccessToken(loginResult.getAccessToken());
                String userid = loginResult.getAccessToken().getUserId();
                GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        updateUI();
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "first_name, last_name, email, id");
                graphRequest.setParameters(parameters);
                graphRequest.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.d("Facebook", String.format("Cancel: %s"));
            }

            @Override
            public void onError(FacebookException error) {
                Log.d("FacebookFragment", String.format("Error: %s", error.toString()));
                String title = getString(R.string.error);
                String alertMessage = "Internet connection failed";
                showResult(title, alertMessage);
            }
        });

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null){
                    Toast.makeText(activity, "Exav", Toast.LENGTH_LONG);
                }

            }
        };

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("user");
    }

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(firebaseAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        firebaseAuth.removeAuthStateListener(firebaseAuthListener);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.accounts_button:
                Intent intent = new Intent(this, UsersActivity.class);
                startActivity(intent);
                break;
        }
    }

    private void showResult(String title, String alertMessage) {
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(alertMessage)
                .setPositiveButton(R.string.ok, null)
                .show();
    }

    private void updateUI() {
        boolean enableButtons = AccessToken.getCurrentAccessToken() != null;

        Profile profile = Profile.getCurrentProfile();
        if (profile != null) {
            if (enableButtons && profile != null) {
                new LoadProfileImage(profile_picture).execute(profile.getProfilePictureUri(200,200).toString());
                helloName.setText("Hello " + profile.getFirstName() + "!");
                String userid = databaseReference.push().getKey();
                String profileId = profile.getId();
                userModel = new UserModel();
                userModel.setName(profile.getFirstName());
                userModel.setSurname(profile.getLastName());
                Uri uri = profile.getProfilePictureUri(200,200);
                userModel.setImage(uri.toString());
                userModel.setFollow(false);

                databaseReference.child(profileId).setValue(userModel);

                loginPreferencesEditor.putLong("userStatus", 1);
                loginPreferencesEditor.putString("id", profile.getId());
                loginPreferencesEditor.putString("first_name", profile.getFirstName());
                loginPreferencesEditor.putString("last_name", profile.getLastName());
                loginPreferencesEditor.putString("profile_picture", profile.getProfilePictureUri(200,200).toString());
                loginPreferencesEditor.commit();
            } else {
                Bitmap icon = BitmapFactory.decodeResource(activity.getResources(),R.mipmap.user_image);
                profile_picture.setImageBitmap(ImageHelper.getRoundedCornerBitmap(activity, icon, 200, 200, 200, false, false, false, false));
                helloName.setText(null);
            }
        } else {
        }

    }

    private class LoadProfileImage extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public LoadProfileImage(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... uri) {
            String url = uri[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(url).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            if (result != null) {
                Bitmap resized = Bitmap.createScaledBitmap(result,200,200, true);
                bmImage.setImageBitmap(ImageHelper.getRoundedCornerBitmap(activity,resized,250,200,200, false, false, false, false));
            }
        }
    }

    private void handleFacebookAccessToken(AccessToken token) {
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            Toast.makeText(activity, "Error",Toast.LENGTH_LONG);
                        }

                    }
                });
    }
}
