package com.example.asus.witplex.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.asus.witplex.R;
import com.example.asus.witplex.models.UserModel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class UserListAdapter extends BaseAdapter {
    private Context ctx;
    private LayoutInflater lInflater;
    private List<UserModel> objects;
    private UserModel userModel;

    public UserListAdapter(Context context, List<UserModel> products) {
        ctx = context;
        objects = products;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            view = inflater.inflate(R.layout.user_list_adapter_view, parent, false);
        }

        userModel = getProduct(position);

        Picasso.with(ctx).load(userModel.getImage()).into((ImageView)view.findViewById(R.id.profile_image));
        ((TextView) view.findViewById(R.id.profile_name)).setText(userModel.getName() + " " + userModel.getSurname());
        TextView follower_count = view.findViewById(R.id.folower_count);
        follower_count.setText(userModel.getFollow_count() + " follow");

        final Button button_follow = view.findViewById(R.id.button_follow);
        button_follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                button_follow.setBackgroundResource(R.drawable.button_style);
                button_follow.setText("Follow");
            }
        });
        return view;
    }

    private UserModel getProduct(int position) {
        return ((UserModel) getItem(position));
    }
}
